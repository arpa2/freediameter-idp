# Realm Crossover in Diameter

> *Diameter has teh capability of dynamic connections
> between servers for different domains/realms.  This
> is how we intend to do it in freeDiameter.*

## What the RFC Says

**Routing.**
[Routes](https://tools.ietf.org/html/rfc6733#section-6.1) are sequences of `DiameterIdentity` values, expressed in AVPs.  When a [`Destination-Host`](https://tools.ietf.org/html/rfc6733#section-6.5) is available, this is the overriding target; if it is absent the [`Destination-Realm`](https://tools.ietf.org/html/rfc6733#section-6.6) is used for a lookup.  Answers refer to the [`Origin-Host`](https://tools.ietf.org/html/rfc6733#section-6.3) and [`Origin-Realm`](https://tools.ietf.org/html/rfc6733#section-6.4) AVPs.  The [`Route-Record`](https://tools.ietf.org/html/rfc6733#section-6.7.1) AVP can be used to collect the `Origin-Host` values along complex routes.  Finally, for Diameter clients, the value of the [Destination-Realm AVP MAY be extracted](https://tools.ietf.org/html/rfc6733#section-6.1) from the User-Name AVP, or other methods; for SASL over Diameter, the server defines a default realm and crossover mechanisms like `SXOVER-PLUS` may override it.

**Resolution.**
To resolve a `Destination-Realm` into its (incoming) `Destination-Host`, we need to consult DNS.  The general mechanism starts with [S-NAPTR records](https://tools.ietf.org/html/rfc6733#appendix-B) followed by SRV records to derive a host and port, along with priority and weight to select among alternative options.  S-NAPTR records are far from common, far from loved and extra indirections may facilitate insecurity, so we are hesitant about them without DNSSEC, but they are formally required for [Diameter peer discovery](https://tools.ietf.org/html/rfc6733#section-5.2).  In lieu of S-NAPTR records, there is a fallback default, so the next stage is always the lookup of SRV records.  We always look for `_diameters._sctp` prefixed to the realm name.

**Certificates.**
Domains used in S-NAPTR and SRV records [occur in the certificate](https://tools.ietf.org/html/rfc6733#section-5.2) for the server, so as to avoid undesired aliases to hijack the Diameter service.  This implies that large-scale hosting can only be flexible about adding new domains without disturbing others if each virtual domain has its own virtual host (as that would occur in the ServerName Indication that selects the service certificate).  When this is done, a change to one domain has only impact on that domain's certificate.  Also, the certificate for one domain then does not list all other hosted domains.

## Requirements for DNSSEC

Diameter acts a bit like a web client; it looks for a name in DNS, connects and validates using TLS.  In that sense, it can live without DNSSEC but is better with it.  The security without DNSSEC can hinge on a root CA that presumably has a path through which it validated the security of the chain.  Having no automation to check if that is true, DNSSEC is certainly better to have (and it is common enough to rely on that).  Clients may be expected to choose to not trade with Diameter nodes that lack DNSSEC security.  *Ultimately, we could validate DNSSEC for all our queries*.

When security data is retrieved from DNS, then it ought to be protected.  DANE should never be trusted without DNSSEC; when security hinges on DANE alone, so without a widely known root CA, then the chain of trust must come from DNS and so DNSSEC is required; when DANE is used as an add-on to retrict the TLS chain of trust it might cause denial-of-service attacks.  DANE indeed says that the DNS information [needs to be protected by DNSSEC](https://tools.ietf.org/html/rfc6698#section-1.3).

## Certificates and/or DANE

Considirations, initial approach and ultimate goals.

**Considerations.**

  * Using certificates with an Extended Key Usage for Diameter-Peer use is a good idea.  It surely should not use TLS-WWW-Server certificates.  *Without this,* certificates may be used that were intended for other services.  *With this,* no current CA will sign your certs [so you'd need to rely on DANE/DNSSEC].
  * The use of realms in SubjectAltName fields DNSname is not very shocking, nor does a current habit exist.  We can simply do this and assume that it will be adopted.  It does answer to the need to validate domains that might be referencing our server name.
  * *Not sure how domains in certificates are to be defined; current validation seems to be that the `Destination-Host` must occur in the `CN` field.  The most likely ploy would be to use the domain in a `DNSname` in the `SubjectAlternateName` certificate extension.  There is [talk of](https://tools.ietf.org/html/rfc6733#section-5.2) an [Extended Key Usage extension](https://tools.ietf.org/html/rfc5280#section-4.2.1.12) but no details and I cannot find an OID for it either.  We may end up defining our own.  Note that CAs will remove such extensions.*

**Initially,**

  * Always use TLS for Realm Crossover.
  * Given a remote realm to address, place it in the `Destination-Realm` AVP.
  * Derive the peer host name from the `Destination-Realm` using SRV records with DNSSEC assurance.
  * Rely on a commonly trusted root CA for certificate validation.
  * Rely on the root CA to have validated the `CN` field in the `Subject`; we will set the peer host name in that field.
  * Rely on the root CA to have validated any `DNSname` fields in the `SubjectAltName` extension; we will set the peer domain name(s) in that field.
  * Send the SNI extension in TLS, set to the peer host name; use that to select the certificate to send from a virtual host; that certificate contains the host name and domain name() supported.  Using SNI improves flexibility for virtual hosting operations of the freeDiameter IdP.
  * Check the remote peer's `CN` and (one of the) `DNSname` to match the `*-Host` and `*-Realm`, respectively; the star is `Origin` or `Destination`.

**Ultimately,**

  * Add DANE/DNSSEC support.  When connecting to a peer, use its host/port combination to validate the peer certificate.  When accepting a peer, use its certificate to determine its server name (from the `CN` in the `Subject` and/or the `Origin-Host` in Diameter) and validate its occurrence in the `Origin-Realm` DNS entries.
  * Allow solely relying on DANE/DNSSEC and not a commonly known root CA.  This would be an option that could service local/inhouse certificate authorities.
  * Find a way to use a Extended Key Usage flag `Diameter-Peer` in spite of it not being supported by current CAs.

## Changes to freeDiameter

Currently, freeDiameter does not support dynamic routing or virtual hosting.  We need to do at least the following:

  * In the SASL plugin, setup the `Destination-Realm` in the first authentication message (session setup) and set it to either the default realm or the realm overridden in the first message if it is for a Realm Crossover mechanism like `SXOVER-PLUS`.
  * For dynamic routing, resolve the `Destination-Realm` to a peer's host and port through SRV records, *ultimately preceded by an S-NAPTR lookup*.
  * For dynamic routing, there needs to be support for the peer's CA and/or a specific certificate.  This may be derived from DANE/DNSSEC, performed on the hostname, protocol and port as derived for dynamic routing.  We may adopt [GnuTLS support for DANE](https://www.gnutls.org/manual/html_node/DANE-API.html) to do this properly.
  * For dynamically routed certificates, we need to have the serviced domain(s) incorporated.
  * Dynamically found peers end up in the peer table, which is already cleaned from stale peers, so freeDiameter actually supports a caching mechanism for active peers.
  * Current Diameter has no support for DTLS over SCTP, but that ought to be trivial.  Their reason is that it was not supported back then (10 years ago).  This has long ago been added to GnuTLS and is the default mode of operation for Diameter.
  * Virtual hosts and realms.  There is a need to support more than one host and more than one realm in the same freeDiameter node.  This is in line with the specifications, but freeDiameterd currently implements only one.  This may be as simple as changing a singular property into a list.  Not all extensions may be prepared to actually function in such a setting, but these can then still be configured with a singular host/realm.
  * SNI-based certificate selection.
